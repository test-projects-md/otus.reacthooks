import React from "react";
import { render } from "@testing-library/react";
import UseStateCounter from "./UseStateCounter";
import Enzyme, { mount } from "enzyme";
//import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
//import renderer from 'react-test-renderer';

//Enzyme.configure({adapter: new Adapter()});

describe("useStateCounter общий тест", () => {
  test("check UseStateCounter", () => {
    const { container } = render(<UseStateCounter />);

    const button = container.getElementsByTagName("button")[0];
    button.click();

    expect(button).toHaveTextContent("У меня useState 1");
  });

  test("Проверяем с enzyme", () => {
    const container = mount(<UseStateCounter />);

    const buttons = container.find(".fancyButton");

    expect(buttons).toHaveLength(1);

    const button = buttons.first();

    button.simulate("click");

    expect(button.text()).toEqual("У меня useState 1");
  });
  // //
  // //
  // //
  // //
  test("Проверяем снапшот", () => {
    //const container = renderer.create(<UseStateCounter/>).toJSON();
    //expect(container).toMatchSnapshot();
  });
});
