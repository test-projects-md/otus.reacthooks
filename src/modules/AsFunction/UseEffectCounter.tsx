import { useEffect, useState } from "react";

const UseEffectCounter = (props: any) => {
  const [i, setI] = useState<number>(0);
  const [j, setJ] = useState<number>(0);
  const [c, setC] = useState<number>(0);

  useEffect(() => {
    console.log("%cЯ работаю после каждого рендера", "background: #222; color: #bada55");
  });

  useEffect(() => {
    console.log(
      "%cЯ сработал один раз",
      "background: red; color: white",
      "background:antiquewhite"
    );
    console.log(
      "%cЯ сработал ТОЖЕ один раз",
      "background: blue; color: white",
      "background:antiquewhite"
    );

    return () => {
      console.log("%cЯ УДАЛИЛСЯ", "background: #222; color: #bada55");
    };
  }, []);

  useEffect(() => {
    console.log(
      "%c Я срабатываю когда меняется i ИЛИ j",
      "background: purple; color: white !important"
    );
  }, [i, j]);

  useEffect(() => {
    console.log("Я срабатываю когда меняется только j");
  }, [j]);
  useEffect(() => {
    console.log("Я срабатываю когда меняется только CCCCCCCC");
  }, [c]);

  const incrI = () => setI(i + 1);
  const incrJ = () => setJ(j + 1);
  const incrC = () => setC(c + 1);

  return (
    <div>
      <button onClick={incrI}>Я меняю I = {i}</button>
      <button onClick={incrJ}>Я меняю J = {j}</button>
      <button onClick={incrC}>Я меняю C = {c}</button>
    </div>
  );
};

export default UseEffectCounter;
