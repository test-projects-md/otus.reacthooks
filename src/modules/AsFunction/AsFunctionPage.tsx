import UseEffectCounter from "./UseEffectCounter";
import { FunCounter } from "./ClassCounter";
import UseStateCounter from "./UseStateCounter";
import { useState } from "react";

const AsFunctionPage = (props: any) => {
  const [close, setClose] = useState<boolean>(false);
  return (
    <div>
      <button onClick={() => setClose(!close)}>Закрыть</button>

      {!close && <UseEffectCounter />}
      {/* <UseStateCounter /> */}
    </div>
  );
};
export default AsFunctionPage;
