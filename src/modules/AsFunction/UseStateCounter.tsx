import { useState } from "react";
import "./style.css";

const UseStateCounter = () => {
  console.log("rerender");
  let [count, setCount] = useState<number>(0);

  const incr = () => {
    console.log("currentCount" + count);
    setCount(count + 1);
  };
  return (
    <>
      <button className='fancyButton' onClick={incr}>
        У меня useState {count}
      </button>
      <button className='fancyButton2' onClick={incr}>
        У меня useState {count}
      </button>
    </>
  );
};

export default UseStateCounter;
