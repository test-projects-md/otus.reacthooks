import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Nav, Navbar } from "react-bootstrap";
import { Route, BrowserRouter as Router, Routes } from "react-router-dom";
import "./App.css";
import AsFunctionPage from "./modules/AsFunction/AsFunctionPage";
import CustomHookPage from "./modules/AsFunction/CustomHookPage";
import UseCallbackPage from "./modules/AsFunction/UseCallbackPage";
import ContextDemo from "./modules/ContextDemo/ContextDemo";
import MuiPage from "./modules/Mui/MuiPage";

function App() {
  return (
    <Router>
      <div className='App'>
        <Navbar bg='light' expand='lg'>
          <Container>
            <Navbar.Brand href=''>React-Bootstrap</Navbar.Brand>
            <Navbar.Toggle aria-controls='basic-navbar-nav' />
            <Navbar.Collapse id='basic-navbar-nav'>
              <Nav className='me-auto'>
                <Nav.Link className='App-link' href='/context'>
                  Контекст
                </Nav.Link>
                <Nav.Link className='App-link' href='/hooks'>
                  Хуки обычные
                </Nav.Link>
                <Nav.Link className='App-link' href='/mui'>
                  MUI
                </Nav.Link>
                <Nav.Link className='App-link' href='/callback'>
                  useCallback
                </Nav.Link>
                <Nav.Link className='App-link' href='/custom'>
                  Custom hooks
                </Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>

        <Routes>
          <Route path='/context' element={<ContextDemo />} />
          <Route path='/hooks' element={<AsFunctionPage />} />
          <Route path='/mui' element={<MuiPage />} />
          <Route path='/callback' element={<UseCallbackPage />} />
          <Route path='/custom' element={<CustomHookPage />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
